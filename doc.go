// Package gcn3 and its subpackages provides a emulator and a detailed timing
// simulator for GCN3-based AMD GPUs.
package gcn3
